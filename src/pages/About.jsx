import React from "react";
import MetaDecorator from "../components/MetaDecorator";

const About = () => {
	return (
		<>
			<MetaDecorator description={"This is about page"} title={"About"} />
			<h1>About</h1>
			<p>This is about page</p>
		</>
	);
};

export default About;
