import React from "react";
import MetaDecorator from "../components/MetaDecorator";

const Home = () => {
	return (
		<>
			<MetaDecorator description={"this is home page"} title={"Home"} />
			<h1>Home page</h1>
			<p>This is home page</p>
		</>
	);
};

export default Home;
