import "./App.css";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import { HelmetProvider } from "react-helmet-async";

function App() {
	return (
		<HelmetProvider>
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/about" element={<About />} />
				{/* Add more routes as needed */}
			</Routes>
		</HelmetProvider>
	);
}

export default App;
